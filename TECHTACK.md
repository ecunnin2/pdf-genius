#The Technical Stack

## PDF.js
Link: https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.2.228/pdf.min.js

This open source tool was essential in taking the pdf file that was loaded from the 
computer and presenting it in the html DOM. The data from the file would be stored in 
a PDF object that would contain data such as the _CURRENT_PAGE, _TOTAL_PAGES, as well 
as the data specific to each page of the document. 

## HTML2Canvas
https://html2canvas.hertzen.com/

This open source tool was essential to taking a screenshot of the pdf file. 
HTML2Canvas would capture part of the html document and generate a canvas 
element. The bytes of that element would then be used to generate an image element.
those bytes are also stored in the card_data that is in the cards dictionary.

## In Combination
Using the two open source tools to achieve the results is quite simple.
First we use PDF.js to load and display the PDF. This handles eveverything 
related to displaying the PDF. We then use the HTML2Canvas to capture the screen
image. We use some sort of key/mouse combination to engage the screen capture to
obtain a set of coordinates that act as the window of capture. We then use the 
HTML2Canvas to create a image of everything on the screen. Once we have that 
image, we can use the coordinates to select and save only part of that larger 
image. With that smaller image, we can extract the data bytes and store the image
accordingly.